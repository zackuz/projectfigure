<div id="test">
    <div id="tesField" class="jumbotron">
        <h2 id="value" class="text-center h2">Empty</h2>
    </div>
    <div class="form-inline">
        <button class="form-control col-md-2" id="retry">Перепроверить</button>
        <button class="form-control col-md-2" id="show">Не помню</button>
        <button class="form-control col-md-2" id="next">Следующий</button>
    </div>
</div>
<div id="outTest">
    <button class="form-control col-md-2" id="singleTest">Повторить прогон</button>
    <button class="form-control col-md-2" id="reversTest">Повторить обратный прогон</button>
</div>

<script>
    var EntityData = <?php echo json_encode($Model['DATA']); ?>;
    var EntityRevers = <?php echo json_encode($Model['REVERS']); ?>;
</script>
<?php
$title = 'Test';
$Scripts[] = 'Scripts/EntityTest.js';

