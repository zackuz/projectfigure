<?php
$title = "Test page";
?>

<h1>Index</h1>
<p>
<?php
   class SomeClass
   {
       public function Hello(){
           echo 'Hello from SomeClass';
       }
   }
   
   class SecondClass extends SomeClass{
       public function Hello(){
           echo 'Hello from SecondClass';
       }
   }
   
   class OtherClass{
       public function Hello(){
           echo 'Hello from OtherClass';
       }
   }
   
   $a = new SecondClass();
   
   var_dump ($a instanceof SomeClass);
   echo '<br>';
   var_dump ($a instanceof SecondClass);
   echo '<br>';
   var_dump ($a instanceof OtherClass);
   echo '<br>';
?>
</p>

