<?php 
    global $User;
    $authenticated = $User->Id!=0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="<?php echo URI_ROOT ?>Content/Icon/brain-64.png">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
               
        <title><?php echo $title; ?></title>
        
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/ct-paper.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/examples.css">


        
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    
    </head>
    <body>
<!--        <div class="navbar navbar-ct-transparent" id="register-navbar">-->
        <nav class="navbar navbar-relative navbar-ct-transparent" role="navigation-demo" id="register-navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                    <div class="logo-container">
                        <div class="logo"><a href="<?php echo URI_ROOT; ?>">
                            <img src="<?php echo URI_ROOT ?>Content/Icon/brain-64.png" alt="Magpie logo"></a>
                        </div>
                    </div>
            </div>
            <div class="collapse navbar-collapse" id="navigation-example-2">
                
        <?php  if ($authenticated): ?>
            <ul class="nav navbar-nav">
                <li><a href="<?php echo URI_ROOT ?>Sets/">Списки</a></li>
                <li><a href="<?php echo URI_ROOT ?>Subjects/">Темы</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo URI_ROOT ?>Account/Profile/">Hello <?php echo $User->Name; ?>!</a></li>
                <li><a href="<?php echo URI_ROOT ?>Account/Logout/">Log off</a></li>
            </ul>

        <?php else: ?>
            <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo URI_ROOT ?>Account/Login/">Авторизация</a></li>
                    <li><a href="<?php echo URI_ROOT ?>Account/Register/">Регистрация</a></li>
            </ul>
                
        <?php endif   ?>      
                    

            </div>
        </div>
    </nav>
<!--    <div class="container body-content">-->
        <?php
            echo $Content;
        ?>
<!--    </div>-->
    <hr />
    <div class="container body-content">
        <footer>
            <p>&copy; 2015</p>
        </footer>
    </div>
    
    <script src="<?php echo URI_ROOT ?>Scripts/jquery-1.10.2.min.js"></script>
    <script src="<?php echo URI_ROOT ?>Scripts/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>  
    <script src="<?php echo URI_ROOT ?>Scripts/bootstrap.min.js"></script>
    
    
    <!--  Plugins -->
    <script src="<?php echo URI_ROOT ?>Scripts/ct-paper-checkbox.js"></script>
    <script src="<?php echo URI_ROOT ?>Scripts/ct-paper-radio.js"></script>
    <script src="<?php echo URI_ROOT ?>Scripts/bootstrap-select.js"></script>
    <script src="<?php echo URI_ROOT ?>Scripts/bootstrap-datepicker.js"></script>
    
    <script src="<?php echo URI_ROOT ?>Scripts/ct-paper.js"></script>
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	
    </body>
</html>

