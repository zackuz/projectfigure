<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="<?php echo URI_ROOT ?>Content/Icon/brain-64.png">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
               
        <title><?php echo $title; ?></title>
        
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/ct-paper.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/demo.css">
        <link rel="stylesheet" type="text/css" href="<?php echo URI_ROOT ?>Content/css/examples.css">


        
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    
    </head>
    <body>
        <nav class="navbar navbar-ct-transparent navbar-fixed-top" role="navigation-demo" id="register-navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <div class="logo-container">
                        <div class="logo"><a href="<?php echo URI_ROOT; ?>">
                            <img src="<?php echo URI_ROOT ?>Content/Icon/brain-64.png" alt="Magpie logo"></a>
                        </div>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navigation-example-2">
                    <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo URI_ROOT ?>Account/Login/">Авторизация</a></li>
                    <li><a href="<?php echo URI_ROOT ?>Account/Register/">Регистрация</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-->
        </nav>
        
        <?php
            echo $Content;
        ?>
        
        
        
        
        
        
        
        
        
    </body>
</html>