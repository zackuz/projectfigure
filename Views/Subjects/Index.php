<?php 
    $title = 'Тэмы спіскаў';
    $uriCtrl =URI_ROOT.'Subjects/';
?>

<h2>Спіс тэм</h2>

<?php if(count($Model)>0): ?>
<a class="btn btn-info" href="<?php echo $uriCtrl.'Create/' ?>">Дадаць новую тэму</a>
<?php foreach ($Model as $item): ?>

<h4><?php echo $item->Name; ?></h4>
<p><?php echo $item->Description; ?></p>
<a class="btn btn-success btn-sm" href="<?php echo $uriCtrl.'Edit/'.$item->Id ?>">Змяніць</a>
<a class="btn btn-danger btn-sm" href="<?php echo $uriCtrl.'Delete/'.$item->Id ?>">Выдаліць</a>

<?php endforeach; ?>
<?php else: ?>
<p>У Вас няма тэм. Дабаўце некалькі.</p>
<a class="btn btn-info" href="<?php echo $uriCtrl.'Create/' ?>">Дадаць новую тэму</a>
<?php endif; ?>




