<?php
    $title = '';
?>
<div class="wrapper">
        <div class="register-background"> 
            <div class="filter-black"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ">
                            <div class="register-card">
            <form method="POST">
                <h2>Регистрация</h2>   
                <?php if(isset($Model['errors']['ALL'])): ?>
                <div class='text-danger'>
                    <ul>
                    <?php foreach ($Model['errors']['ALL'] as $err): ?>
                    <li><?php echo $err; ?></li>
                    <?php endforeach ?>
                    </ul>
                </div>
                <?php endif ?>
                <label>Лагін</label>
                <input  name="RegLogin" type="text" class="form-control" placeholder="Лагін">
                
                <label>Пароль</label>
                <input name="RegPassword" type="password" class="form-control" placeholder="Пароль">
                
                <label>Пацвярдженне паролю</label>
                <input name="PasswordConfirm" type="password"  class="form-control" placeholder="Пацвярдженне пароля..."/>
                
                <label>Імя</label>
                <input name="RegName" type="text" class="form-control" placeholder="Пароль">
                
                <input name="BackUri" type="hidden" value="<?php echo $Model['backUri'] ?>" />
                <div class='form-group'>
                <button class="btn btn-danger btn-block" name="go" type="submit">Зарэгістравацца</button>
                </div>
            </form>
                                <div class="forgot">
                                    <a href="#" class="btn btn-simple btn-danger">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
        </div>
    </div> 