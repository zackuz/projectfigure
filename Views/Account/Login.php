<?php
    $title = 'Авторизация';
?>

<div class="wrapper">
        <div class="register-background"> 
            <div class="filter-black"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ">
                            <div class="register-card">
                                <h3 class="title">Прывітанне</h3>
                                <form class="register-form" method="POST">
                                    <?php if(isset($Model['errors']['ALL'])): ?>
                                    <div class='text-primary'>
                                        <ul>
                                        <?php foreach ($Model['errors']['ALL'] as $err): ?>
                                        <li><?php echo $err; ?></li>
                                        <?php endforeach ?>
                                        </ul>
                                    </div>
                                    <?php endif ?>
                                    <label>Лагін</label>
                                    <input  name="Login" type="text" class="form-control" placeholder="Лагін">

                                    <label>Пароль</label>
                                    <input name="Password" type="password" class="form-control" placeholder="Пароль">
                                    <input name="BackUri" type="hidden" value="<?php echo $Model['backUri'] ?>" />
                                    <button class="btn btn-danger btn-block">Вайсці</button>
                                </form>
                                <div class="forgot">
                                    <a href="#" class="btn btn-simple btn-danger">Forgot password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
        </div>
    </div>    