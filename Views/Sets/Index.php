<?php $title='Наборы'; $counter=0; $Scripts[] = 'Scripts/CheckboxesGroupes.js'; ?>

<h3>Спіс набораў</h3>
<form method="POST" name='TestRequest' action="<?php echo URI_ROOT; ?>Test/">
    
<?php if(isset($Model)): ?>
<table>
    
    <thead>
        <tr>
            <td><label class="checkbox checkbox-red"><input id="1" type="checkbox" data-toggle="checkbox" data-group-main="list"></label></td>
            <td colspan="3">
                <button type="submit" name="Action" value="Test" class="btn btn-success" data-grope-enebled="list">Прагон</button>
                <button type="submit" name="Action" value="Revers" class="btn btn-success" data-grope-enebled="list">Абратны прагон</button>
                <a href="<?php echo URI_ROOT; ?>Sets/Create/" class="btn btn-info">Дадаць новы спіс</a>
                <button type="submit" name="Action" value="Delete" class="btn btn-danger" data-grope-enebled="list">Выдаліць абраныя</button>    
            </td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Model as $subject => $sets): ?>
        <tr>
            
            <td colspan="4"><h6><?php echo $subject; ?></h6></td>
        </tr>
            <?php foreach ($sets as $item): ?>
            <?php $counter++; ?>
            <tr>
                <td><label class="checkbox checkbox-azure">
                        <input name="Id[]" value="<?php echo $item->Id; ?>" type="checkbox" data-toggle="checkbox" data-group="list">
                    </label></td>
                <td>
                    <h6 class="text text-info"><?php echo $item->Name; ?></h6>
                    <p><?php echo substr($item->Description, 0,100).'...'; ?></p>
                    <p>
                        <a class="btn btn-success btn-xs" href="<?php echo URI_ROOT.'Test/Avers/'.$item->Id ?>">Прагон</a>
                        <a class="btn btn-success btn-xs" href="<?php echo URI_ROOT.'Test/Revers/'.$item->Id ?>">Абратны прагон</a> 
                        <a class="btn btn-info btn-xs" href="<?php echo URI_ROOT.'Sets/View/'.$item->Id ?>">Агляд</a>
                        <a class="btn btn-info btn-xs" href="<?php echo URI_ROOT.'Sets/Edit/'.$item->Id ?>">Змяніць</a>
                        <a class="btn btn-danger btn-xs" href="<?php echo URI_ROOT.'Sets/Delete/'.$item->Id ?>">Выдаліць</a> 

                    </p>
                </td>
                <td></td>
                <td>
<!--                    <a class="btn btn-success btn-sm" href="<?php echo URI_ROOT.'Sets/Edit/'.$item->Id ?>">Змяніць</a>
                    <a class="btn btn-danger btn-sm" href="<?php echo URI_ROOT.'Sets/Delete/'.$item->Id ?>">Выдаліць</a> 
            -->    </td>
            
            </tr>

            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
    <?php if($counter>20): ?>
    <tfoot>
        <tr>
            <td><label class="checkbox checkbox-red"><input type="checkbox" data-toggle="checkbox"></label></td>
            <td colspan="3">
                <button type="submit" name="Action" value="Test" class="btn btn-success">Прагон</button>
                <button type="submit" name="Action" value="Revers" class="btn btn-success">Абратны прагон</button>
                <a href="<?php echo URI_ROOT; ?>Sets/Create/" class="btn btn-info">Дадаць новы спіс</a>
                <button type="submit" name="Action" value="Delete" class="btn btn-danger" disabled="disabled">Выдаліць абраныя</button>    
            </td>
        </tr>
    </tfoot>
    <?php endif; ?>
</table>
<?php else: ?>
<p>_________</p>
<a href="<?php echo URI_ROOT; ?>Sets/Create/" class="btn btn-info">Дадаць новы спіс</a>
                

<?php endif; ?>


</form>

<script>

//     el = document.getElementById('1');
//     el.checked = true;
//     el.indeterminate = true;    
     
</script>

