<h2><?php echo $Model['INFO']->Name; ?></h2>
<p><?php echo $Model['INFO']->Description; ?></p>

<div id="setEditTool">
    <button class="btn btn-primary" id="showAddForm">Дадаць новыя пары</button>
    <a href='<?php echo URI_ROOT."Test/Avers/".$Model['INFO']->Id; ?>' class="btn btn-info">Прагон</a>
    <a href='<?php echo URI_ROOT."Test/Revers/".$Model['INFO']->Id; ?>' class="btn btn-info">Revers</a>
    <a href='<?php echo URI_ROOT."Sets/Join/".$Model['INFO']->Id; ?>' class="btn btn-info">Join to</a>
</div>
<div id="AddForm" class='hidden'>
<div class='register-card wide' style="max-width: auto;">
    <button class="btn btn-primary" id="hideAddForm">Згарнуць форму</button>
        <button class="btn btn-success" id="addPairBtn">Новыя пары</button>
        <button class="btn btn-info" id="formSubmit">Дадаць</button>
    <form class="register-form" method="POST">       
        <div  id="1">     
            <label>Entity</label>
            <input name="Value[1]" type="text" class="form-control">
            <label>Link</label>
            <input name="Link[1]" type="text" class="form-control">
        </div>
        <div  id="template" class="hidden" data-identy="Group">   
            <label>Entity</label>
            <input type="text" class="form-control" data-identy="Value">
            <label>Link</label>
            <input type="text" class="form-control" data-identy="Link">
            <a class="btn btn-warning btn-sm" data-identy="Del">Delete</a>
            <hr>
        </div>
    </form>
</div>
</div>
<table class="table">
    <thead>
        <tr>
            <th>Значенне</th>
            <th>Ассацыяцыя</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Model['LIST'] as $item): ?>
        <tr>
            <td><?php echo $item->Value; ?></td>
            <td><?php echo $item->Link; ?></td>
            <td>
                <a href='<?php echo URI_ROOT."Entity/Edit/$item->Id/".$Model['INFO']->Id; ?>' class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span></a>
                <a href='<?php echo URI_ROOT."Entity/Delete/$item->Id/".$Model['INFO']->Id; ?>' class="btn btn-warning btn-sm">
                    <span class="glyphicon glyphicon-remove"></span></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$title = 'Агляд спіса';
$Scripts[]='Scripts/AddEntity.ui.js';
