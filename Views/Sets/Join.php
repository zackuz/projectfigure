<?php
$title = 'Join list';
?>

<h5>Выберите целевой список</h5>

<?php if(isset($Model)): ?>
<table>
    <thead><tr><td style='width:1.5em;'></td><td></td></tr></thead>
    <tbody>
        <?php foreach ($Model['LIST'] as $subject => $sets): ?>
        <tr>
            
            <td colspan="4"><h6><?php echo $subject; ?></h6></td>
        </tr>
            <?php foreach ($sets as $item): ?>
            <tr>
                <td></td>
                <td>
                    <h6 class="text text-info"><?php echo $item->Name; ?></h6>
                    <p><?php echo substr($item->Description, 0,100).'...'; ?></p>
                </td>
                <td><a class='btn btn-info' href='<?php echo URI_ROOT."Sets/Join/{$Model['ID']}/$item->Id"; ?>'>
                        <span class='glyphicon glyphicon-ok'> </span> Join
                    </a></td>
            </tr>

            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
<p>_________</p>
<a href="<?php echo URI_ROOT; ?>Sets/Create/" class="btn btn-info">Дадаць новы спіс</a>
                

<?php endif; ?>

