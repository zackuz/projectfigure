<?php
//This project use FrontController pattern
//Realise cookie ticket authentication

//Environment
//$_SUB_ENV = [ConfigurationSubFolder];
if(isset($_SUB_ENV)){
    require_once "/Configurations/$_SUB_ENV/Consts.php";
    require_once "/Configurations/$_SUB_ENV/DataBase.php";
}else{
    require_once './Configurations/Consts.php';
    require_once './Configurations/DataBase.php';    
}

//Helpers
require_once './Components/Helpers/echo.php';



//Including section

require_once './Components/Error/Error.php';
require_once './Components/Route/Router.php';
require_once './Components/Route/RouteModel.php';
require_once './Components/Route/RouteBinder.php';
require_once './Components/Authenticate/UserModel.php';
require_once './Components/Authenticate/Authorization.php';



//Enable displaying errors
//Comment it for realise
ini_set('display_errors', 1);
error_reporting(E_ALL);



//Authentication
global $User;
$User = Authorization::CheckUser();


//Prepeare route
global $Route;
$Route = RouteBinder::Bind($_SERVER['REQUEST_URI']);


//Call detected action
$router = new Router();
$router ->run();




