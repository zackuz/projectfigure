<?php

class SetModel {
    public $Id;
    public $Name;
    public $Description;
    public $SubjectId;
    
    public function __construct($id=null, $name=null, $description=null, $subjectId = null) {
        $this->Id=$id;
        $this->Name=$name;
        $this->Description=$description;
        $this->SubjectId = $subjectId;
    }
}
