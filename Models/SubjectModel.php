<?php

class SubjectModel {
    public $Id;
    public $Name;
    public $Description;
    //public $Lists;
    
    public function __construct($id=null, $name=null, $description=null) {
        $this->Id=$id;
        $this->Name=$name;
        $this->Description=$description;
    }
}
