<?php
include_once ROOT.'/Components/RequestResults/View.php';
include_once ROOT.'/Components/RequestResults/Redirect.php';
include_once ROOT.'/Components/RequestResults/Text.php';
include_once ROOT.'/Components/RequestResults/DataInfo.php';
include_once ROOT.'/Components/RequestResults/Json.php';
include_once ROOT.'/Components/RequestResults/JsonFile.php';
include_once ROOT.'/Components/Data/Connection.php';

class Router {
    
    public function run() {
        global $Route;
        $trigger;
        $currentControllerName = $Route->Controller .'Controller';
        $controllerFileName = ROOT.'/Controllers/'.$currentControllerName .'.php';
        if (file_exists($controllerFileName)){
            include_once ($controllerFileName);
            $currentController = new $currentControllerName;
            if (method_exists($currentController, $Route->Action)){
                $trigger = call_user_func_array(array($currentController, $Route->Action), $Route->Parameters);
                //$view->Execute();
            }
            else{
                //Add Error
                echo "Error!!! Action not found!!!";
            }            
        }
        else{
            //Add Error
            echo "Error!!! Controller not found!!!";
        }      
        
        if (Error::Is($trigger)){
            var_d($trigger);
        }
        else {
            $trigger->Execute();
        }
        
    }
}
