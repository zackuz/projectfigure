<?php
class RouteModel {
    public $Controller;
    public $Action;
    public $Parameters;
    
    public function Init($controller, $action, $parameters){
        $this->Controller = $controller;
        $this->Action = $action;
        $this->Parameters = $parameters;
    }
}
