<?php

class RouteBinder {
    static function Bind($uri=''){
        $route = new RouteModel();
        if(!empty($uri)){
            $uri = explode('?', $uri)[0];//Cut address parameters
            $uri = ltrim($uri, URI_ROOT);//Cut root 
            $uri = rtrim($uri,'/');//Cut last slash
            $uri = explode('/', $uri);//Get uri segments
            
            //Analyze segments. Almoust .Net schema :)
            $count = count($uri);
            $params = array();
            if($count==0 || $uri[0]==''){
                $route->Init(DEFAULT_CONTROLLER, DEFAULT_ACTION, $params);
                
            }           
            elseif($count==1){
                $controller = ucfirst(strtolower(array_shift($uri)));
                $route->Init($controller, DEFAULT_ACTION, $params);
            }
            else{
                $controller = ucfirst(strtolower(array_shift($uri)));
                $action =  ucfirst(strtolower(array_shift($uri)));
                if ($count>2){
                    $params = $uri;
                }
                $route ->Init($controller, $action, $params);
            }
        }
        else {
            $route->Init(DEFAULT_CONTROLLER, DEFAULT_ACTION, array());
        }
        return $route;
    }
}
