<?php
include_once 'View.php';
class DataInfo extends View{
    protected $layoutPath;
    protected $pagePath;
    //protected $sections;
    protected $model;
    
    
    public function __construct($model=null, $page=null, $layout = LAYOUT) {
        global $Route;
        $this->model = $model;
        $this->pagePath = is_null($page)?
                ROOT."/Views/$Route->Controller/$Route->Action.php":$page;
        $this->layoutPath = ROOT."/Views/Layouts/$layout.php";      
    }
    
    public function Execute(){
        ob_start();
        var_d($this->model);
        $Content = ob_get_contents();
        ob_end_clean();
        $title='Data info';
        include ($this->layoutPath);
    }

//put your code here
}
