<?php
include_once 'IRequestResult.php';
class Redirect implements IRequestResult{
    private $uri;
    
    public function __construct($uri) {
        $this->uri = $uri;
    }

    public function Execute() {
        header('Location: '.$this->uri);
        die();
    }

}
