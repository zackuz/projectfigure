<?php
include_once 'IRequestResult.php';

class Json implements IRequestResult{
    
    protected $data;
    //protected $fileName;


    public function __construct($data) {
        $this->data = $data;
        //$this->fileName = isset($fileName)?$fileName:'base.json';
    }
    public function Execute() {
        header('Content-Type: application/json');
        echo json_encode($this->data);    
    }

}