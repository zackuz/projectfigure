<?php
include_once 'View.php';
class Text extends View{
    private $text;
    
    public function __construct($text, $model=null, $layout=null) {
        $this->text = $text;
        global $Route;
        $this->model = $model;
        $this->layoutPath = is_null($layout)?
                ROOT."/Views/Layouts/Default.php":$layout;
    }
    public function Execute(){;
        $title='Text';
        $Content = $this->text;
        include ($this->layoutPath);
    }

//put your code here
}
