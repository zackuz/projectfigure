<?php

interface IRequestResult {
    public function Execute();
}
