<?php
include_once 'Json.php';

class JsonFile extends Json{
    
    protected $fileName;


    public function __construct($data, $fileName = null) {
        $this->data = $data;
        $this->fileName = isset($fileName)?$fileName:'base.json';
    }
    public function Execute() {
        header('Content-Type: file/json');
        header('Content-Disposition: attachment; filename='.$this->fileName);
        echo json_encode($this->data);    
    }

}

