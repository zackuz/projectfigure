<?php
include_once 'IRequestResult.php';

class View implements IRequestResult{
    protected $layoutPath;
    protected $pagePath;
    //protected $sections;
    protected $model;
    
    
    public function __construct($model=null, $page=null, $layout = LAYOUT) {
        global $Route;
        $this->model = $model;
        $this->pagePath = is_null($page)?
                ROOT."/Views/$Route->Controller/$Route->Action.php":$page;
        $this->layoutPath = ROOT."/Views/Layouts/$layout.php";      
    }
    
    public function Execute(){
        $PagePath = $this->pagePath;
        $Model = $this->model;
        ob_start();
        include($PagePath);
        $Content = ob_get_contents();
        ob_end_clean();
        include ($this->layoutPath);
    }
}
