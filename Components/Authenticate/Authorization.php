<?php

require_once ROOT.'/Components/Authenticate/AuthCookie.php';
require_once ROOT.'/Components/Authenticate/Crypto.php';
require_once ROOT.'/Components/Authenticate/TicketModel.php';
require_once ROOT.'/Components/Data/UserService.php';
require_once ROOT.'/Components/Authenticate/RegisterModel.php';

class Authorization{
    static function CheckUser() {
        $user = new UserModel();
        if(AuthCookie::is_Exist()){
            $ticket = AuthCookie::get();
            $user = UserServise::isRegister($ticket->Login, $ticket->PasswordHash);
            if ($user->Id < 0 ) {
                $user = new UserModel();
                $user->InitAnnonim();               
            }
            else{
                //Authcookie refresh
                $ticket->Expiration = self::expireTime($ticket->Remember);
                AuthCookie::set($ticket, $ticket->Expiration);
            }
        }
        else {
            $user->InitAnnonim();
        }
        return $user;
    }

    static function Login($login, $password, $remember){
        global $User;
        $passwordHash = self::HashPassword($password);
        $User = UserServise::isRegister($login, $passwordHash);
        
        if (is_bool($User)) {
            $User=new UserModel();
            $User->InitAnnonim();
        }
        
        if ($User->Id>0){
            $ticket = new TicketModel();
            $ticket->Id = 2;
            $ticket->Login = $login;
            $ticket->Name = $User->Name;
            $ticket->PasswordHash = $passwordHash;
            $ticket->Expiration = self::expireTime($remember);
            $ticket->Remember = $remember;

            AuthCookie::set($ticket, $ticket->Expiration);
            return true;
        }
        else{
                return false;
        }
    }

    static function Logout(){
            AuthCookie::delete();
    }
    
    
    //Old code
//    static function Register($login, $password){
//            global $user;
//            $userModel = new UserModel();
//            $userModel->Init(0, '', $login, self::HashPassword($password));
//            $service = new UserService();
//            $id = $service->RegisterUser($userModel);
//            if ($id==0) return false;
//            $user->Init($id, $userModel->Login, '', ':)');
//            echo 'AuthModule';
//		//New ticket
//            $ticket = new TicketModel();
//            $ticket->Id = $user -> Id;
//            $ticket->Login = $user -> Login;
//            $ticket->Name = $user -> Name;
//            $ticket->Expiration = time() + (($remember ? 432000 : 1200));
//            //$encryptTicket = json_encode($ticket);
//
//            AuthCookie::set($ticket, $ticket->Expiration);
//            return true;
//    }
    
    static function RegisterAndLogin($registerModel){
        $user = new UserModel();
        $user->Name = $registerModel->Name;
        $user->Login = $registerModel->Login;
        $user->PasswordHash = self::HashPassword($registerModel->Password);
        if (UserServise::Register($user)){
            self::Login($registerModel->Login, $registerModel->Password, false);
        };
        //Register
        
        
    }
    
    static function DennyAnnonim(){
            global $User;
            if ($User->Id == 0){
                //Need to insert error redirect obect
                header('Location: '.URI_ROOT.LOGIN_PAGE.'?back='.$_SERVER['REQUEST_URI']);
                die();
            }
    }

    private static function HashPassword($password){
            //return $password;
            return md5($password);
    }
    
    private static function expireTime($remember){
        return time() + ($remember ? 432000 : 1200);
    }
}