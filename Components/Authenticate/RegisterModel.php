<?php

class RegisterModel {
    public $Name;
    public $Login;
    public $Password;
    public $PasswordConfirm;
    
    public static function Validate($login, $name, $password, $passwordConfirm){
        //Validation
        $data = new RegisterModel();
        $data->Login = $login;
        $data->Name = $name;
        $data->Password = $password;
        $data->PasswordConfirm = $passwordConfirm;
        return $data;
    }
    
}
