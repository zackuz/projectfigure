<?php

class AuthCookie {

	const authCookieName = 'ticket';

	public static function is_Exist(){
		return isset($_COOKIE[self::authCookieName]);
	}

	public static function set($value, $expire){
		setcookie(self::authCookieName, Crypto::Encrypt($value), $expire, URI_ROOT);
	}

	public static function get(){
		return Crypto::Decrypt($_COOKIE[self::authCookieName]);
	}

	public static function refresh($value, $expire){
		setcookie(self::authCookieName, $value, $expire, URI_ROOT);
	}

	public static function delete(){
		if (isset($_COOKIE[self::authCookieName])){
			setcookie(self::authCookieName, '', time() - 3600, URI_ROOT);
		}
	}
}
