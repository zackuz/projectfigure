<?php

class UserModel
{
	var $Id;
	var $Name;
	var $Login;
	var $PasswordHash;

	public function InitAnnonim()
	{
		$this->Id = 0;
		$this->Name = "Ananimous";
		$this->Login = "";
		$this->PasswordHash = "";
	}

	public function Init($id, $name, $login, $passwordHash)
	{
		$this->Id = $id;
		$this->Name = $name;
		$this->Login = $login;
		$this->PasswordHash = $passwordHash;
	}
}
