<?php
include_once ROOT.'/Components/Data/Connection.php';
include_once ROOT.'/Models/SubjectModel.php';

class SubjectService {
    
    public static function AllForUser()
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select Id, Name, Description from subjects where UserId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');
        $query->execute(array($User->Id));
        //$data = $query->fetchAll();
        return $query->fetchAll();
    }
    
    public static function Create($name, $description){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("insert into subjects(Name, Description, UserId) values (?, ?, ?);");
        $query->execute(array($name,$description, $User->Id));
    }
    
    public static function ItemById($id)
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select Id, Name, Description from subjects where Id like ? and UserId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');
        $query->execute(array($id, $User->Id));
        //$data = $query->fetchAll();
        return $query->fetch();
    }
    
    public static function Update($id, $name, $description){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("update subjects set Name = ?, Description = ?, UserId = ? where Id like ?;");
        $query->execute(array($name,$description, $User->Id, $id));
    }
    public static function Delete($id)
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("delete from subjects where Id like ? and UserId like ?");
        $query->execute(array($id, $User->Id));
    }
}
