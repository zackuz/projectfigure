<?php

class Connection {
    private static $db;
    public static function Open(){
        if (!isset($db)) {
            try{
                $db = new PDO(DB_CONNECTION, DB_USER, DB_PASSWORD);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $ex) {
                die('DB problem.');
            }
            
        }
        return $db;
    }
    public static function Close(){
        
    }
}
