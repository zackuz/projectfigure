<?php
require_once ROOT.'/Components/Authenticate/RegisterModel.php';

class UserServise {
    public static function isRegister($login, $passwordHash) {
        $db = Connection::Open();

        $query = $db->prepare("select * from users where Login like :login and PasswordHash = :pass;");
        $query->execute(array(':login'=>$login, ':pass'=> $passwordHash));       
        $query->setFetchMode(PDO::FETCH_CLASS, 'UserModel');
        
        return $query->fetch();
    }
    
    public static function Register($userModel){
        try{
            $db = Connection::Open();

            $sql = 'Insert into users(Login, Name, PasswordHash) values (?, ?, ?);';
            $query = $db->prepare('Insert into users(Login, Name, PasswordHash) values (?, ?, ?);');
            $query->execute(array($userModel->Login,$userModel->Name, $userModel->PasswordHash));
        } 
        catch(Exception $e){
            return false;
        }
        
        return true;
        
    }
}
