<?php
    
    include_once ROOT.'/Components/Data/Connection.php';
    include_once ROOT.'/Models/SubjectModel.php';
    include_once ROOT.'/Models/SetModel.php';
    
class SetService {
    public static function AllForUserTest()
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select Id, Name, Description, SubjectId from sets where UserId like ?;select Id, Name from subjects where UserId like ?;");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');
        $query->execute(array($User->Id, $User->Id));
        $data[] = $query->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SetModel');
        $query->nextRowset();
        //$data[] = $query->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_UNIQUE|PDO::, 'SubjectModel');
        $data[] = $query->fetchAll(PDO::FETCH_UNIQUE|PDO::FETCH_COLUMN, 1);
        return $data;
    }
    public static function AllGropeForUser()
    {
        global $User;
        $data=null;
        $db = Connection::Open();
        $query = $db->prepare("select Id, Name from subjects where UserId like ?;select Id, Name, Description, SubjectId from sets where UserId like ?;");
        $query->execute(array($User->Id, $User->Id));
        $subs = $query->fetchAll(PDO::FETCH_UNIQUE|PDO::FETCH_COLUMN, 1);
        if (count($subs)>0){
            $query->nextRowset();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');      
            while( $row = $query->fetch() ) {
                $data[$subs[$row->SubjectId]][] = $row;           
            }
        }
        return $data;
    }
    public static function AllGropeForUserButId($id)
    {
        global $User;
        $data=null;
        $db = Connection::Open();
        $query = $db->prepare("select Id, Name from subjects where UserId like ?;select Id, Name, Description, SubjectId from sets where UserId like ?;");
        $query->execute(array($User->Id, $User->Id));
        $subs = $query->fetchAll(PDO::FETCH_UNIQUE|PDO::FETCH_COLUMN, 1);
        if (count($subs)>0){
            $query->nextRowset();
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');      
            while( $row = $query->fetch() ) {
                if ($row->Id!=$id){
                    $data[$subs[$row->SubjectId]][] = $row;
                }
            }
        }
        return $data;
    }
    
    public static function ItemById($id)
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select * from sets where Id like ? and UserId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SetModel');
        $query->execute(array($id, $User->Id));
        //$data = $query->fetchAll();
        return $query->fetch();
    }
    
    public static function Create($set){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("insert into sets(Name, Description, UserId, SubjectId) values (?, ?, ?, ?);");
        $query->execute(array($set->Name,$set->Description, $User->Id, $set->SubjectId));
    }
    
    public static function Update($set){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("update sets set Name = ?, Description = ?, UserId = ?, SubjectId = ? where Id like ?;");
        $query->execute(array($set->Name,$set->Description, $User->Id, $set->SubjectId, $set->Id));
    }
    
    public static function Delete($id)
    {
        //Add query for delete linked entitys
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("delete from sets where Id like ? and UserId like ?");
        $query->execute(array($id, $User->Id));
    }
    
    public static function Gather($rootId, $sourceId){
        $db = Connection::Open();
        $query = $db->prepare("update entities set SetId = ? where SetId like ?;delete from sets where Id like ?;");
        $query->execute(array($rootId, $sourceId, $sourceId));
    }
}
