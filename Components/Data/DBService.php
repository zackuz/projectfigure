<?php
    include_once ROOT.'/Components/Data/Connection.php';
    include_once ROOT.'/Models/SubjectModel.php';
    include_once ROOT.'/Models/SetModel.php';
    //include_once ROOT.'/Models/SetModel.php';
    
class DBService{
    public static function Export(){
        global $User;
        $db = Connection::Open(); 
        
        
        //Gets SUBJECTS
        $query = $db->prepare("select Id, Name, Description from subjects where UserId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SubjectModel');
        $query->execute(array($User->Id));
        while($row = $query->fetch()){
            $subjects[$row->Id]['Subject'] = $row;
        }
        
        //Gets SETS
        $query = $db->prepare("select Id, Name, Description, SubjectId from sets where UserId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SetModel');
        $query->execute(array($User->Id));
        while($row = $query->fetch()){
            $sets[$row->Id]['Set'] = $row;
        }
        
        //Get ENTITIES
        $query = $db->prepare("select Value, Link, SetId from entities join sets on sets.Id like entities.SetId where sets.UserId like ?");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $query->execute(array($User->Id));
        
        while($row = $query->fetch()){
            $sets[$row['SetId']]['Entities'][] = array('Value' => $row['Value'],'Link' => $row['Link']);
        }
        
        //Put SETS on SUBJECTS
        
        foreach ($sets as $item) {
            //$sublects[$item['Set']->SubjectId]['Sets'][]=$item;
            $subjects[$item['Set']->SubjectId]['Sets'][]=$item;
        }
        $data['Head'] = array('Version' => 1, 'Type' => 'EntyFullDatabase');
        $data['Body'] = array_values($subjects);
        return $data;
    }
    
    public static function ImportJson ($data){
        if (!isset($data)) return new Error('Data is not exist');
        if(!isset($data->Head->Version) || !isset($data->Body)) new Error('Bad data format');
        
        self::ImporJsonVersionOne($data->Body);
        
        return true;
    }
    
    private static function ImporJsonVersionOne($data){
        global $User;
        $db = Connection::Open();
        
        foreach ($data as $item) {
            $subject = $item->Subject;
            $query = $db->prepare("insert into subjects(Name, Description, UserId) values (?, ?, ?);");
            $query->execute(array($subject->Name,$subject->Description, $User->Id));
            $subjectId = $db->lastInsertId();
            
            
            if (isset($item->Sets))
                foreach ($item->Sets as $setItem) {
                    $set = $setItem->Set;
                    $query = $db->prepare("insert into sets(Name, Description, UserId, SubjectId) values (?, ?, ?, ?);");
                    $query->execute(array($set->Name,$set->Description, $User->Id, $subjectId));
                    $setId = $db->lastInsertId();


                    if (isset($set->Entities))
                        foreach ($set->Entities as $entityItem) {
                            $query = $db->prepare("insert into entities (Value, Link, SetId) Values (?, ?, ?);");
                            $query->execute(array($entityItem->Value, $entityItem->Link, $setId));
                        }
                }
        }
    }
}

