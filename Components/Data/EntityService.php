<?php

include_once ROOT.'/Components/Data/Connection.php';
include_once ROOT.'/Models/EntityModel.php';

class EntityService{
    public static function AllForUserBySetId($setId)
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select entities.Id, Value, Link from entities join sets on sets.Id like entities.SetId where UserId like ? and SetId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'EntityModel');
        $query->execute(array($User->Id, $setId));

        return $query->fetchAll();
    }
    
    //If we have set of sets. :)
    public static function AllForUserBySets($setIds)
    {
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select entities.Id, Value, Link from entities join sets on sets.Id like entities.SetId where UserId like ? and SetId like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'EntityModel');
        $query->execute(array($User->Id, $setIds[0]));
        $data = $query->fetchAll();
        
        unset($setIds[0]);
        
        foreach ($setIds as $id) {
            $query = $db->prepare("select entities.Id, Value, Link from entities join sets on sets.Id like entities.SetId where UserId like ? and SetId like ?");
            $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'EntityModel');
            $query->execute(array($User->Id, $id));
            $data = array_merge($data, $query->fetchAll());
        }

        return $data;
    }
    
    public static function ItemById($id){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select entities.Id, Value, Link from entities join sets on sets.Id like entities.SetId where UserId like ? and entities.Id like ?");
        $query->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'EntityModel');
        $query->execute(array($User->Id, $id));
        
        return $query->fetchAll()[0];
    }
    
    public static function InsertValues($values, $links, $setId){
        global $User;
        $db = Connection::Open();
        $query = $db->prepare("select Count(*) as Count from sets where UserId like ? and Id like ?");
        //$query->setFetchMode(PDO::FETCH_ASSOC);
        $query->execute(array($User->Id, $setId));
        $data = $query->fetchAll();
        if ($data[0]['Count']>0){
            foreach ($values as $key => $item) {
                $query = $db->prepare("insert into entities (Value, Link, SetId) Values (?, ?, ?);");
                $query->execute(array($item, $links[$key], $setId));
            }
        }       
    }
    
    public static function Delete($id, $setId){
        global $User;
        $db = Connection::Open();
        if (self::CheckOwner($setId, $User->Id, $db)){
            $query = $db->prepare("delete from entities where Id like ?");
            $query->execute(array($id));
        }
    }
    
     public static function Update ($id, $value, $link, $setId){
        global $User;
        $db = Connection::Open();
        if (self::CheckOwner($setId, $User->Id, $db)){
            $query = $db->prepare("update entities set Value = ?, Link = ?, SetId = ? where Id = ?");
            $query->execute(array($value, $link, $setId, $id));
        }
     }
     
     private static function CheckOwner($setId, $userId, $connection){
        $query = $connection->prepare("select Count(*) as Count from sets where UserId like ? and Id like ?");
        $query->execute(array($userId, $setId));
        $data = $query->fetchAll();
        $file = fopen('C:/log.txt', 'a');
        fwrite($file, "Set - $setId; UserId - $userId \n");
        fwrite($file, "Data".$data[0]['Count']);
        fclose($file);
        return $data[0]['Count']>0;  
     }
}

