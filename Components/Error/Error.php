<?php

class Error{
    protected $type;
    protected $description;
    protected $data;
    protected $inner;
    
    public function __construct($description, $data = null, $inner = null) {
        $this->type = 'Just error';
        $this->description = $description;
        $this->data = $data;
        $this->inner = $inner;       
    }
    
    public static function Is($error){
        return $error instanceof Error;
    }
}

