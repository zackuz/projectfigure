var AddEntity = {
    Count: 1
};
$(document).ready(function(){
    $('#showAddForm').bind('click', function(){
        $('#AddForm').removeClass('hidden');
        $('#setEditTool').addClass('hidden');
    });
    $('#hideAddForm').bind('click', function(){
        $('#AddForm').addClass('hidden');
        $('#setEditTool').removeClass('hidden');
    });
    $('#addPairBtn').bind('click', function(){
        AddEntity.Count +=1;
        var el = $('#template').clone();
        el.insertBefore($('form.register-form').children(':first'));
        el.prop('id', AddEntity.Count);
        el.removeClass('hidden');
        el.children('[data-identy=Value]:first').prop('name', 'Value[' + AddEntity.Count + ']');
        el.children('[data-identy=Link]:first').prop('name', 'Link[' + AddEntity.Count + ']');
        el.children('[data-identy=Del]:first').click(function(){
            $(this).closest('[data-identy=Group]').remove();
        });      
    });
    
     $('#formSubmit').bind('click', function(){
         $('form.register-form').submit();
     });
});


