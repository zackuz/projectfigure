<?php
    include_once ROOT.'/Models/SubjectModel.php';
    include_once ROOT.'/Components/Data/SubjectService.php';
    
    Authorization::DennyAnnonim();
    
class SubjectsController {
    
    public function Index(){
        Authorization::DennyAnnonim();
        $data = SubjectService::AllForUser();
        return new View($data);
    }
    
    public function Create(){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            SubjectService::Create($_POST['Name'], $_POST['Description']);
            return new Redirect(URI_ROOT.'Subjects/');
        }
        
        return new View();
    }
    
    public function Edit($id){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            SubjectService::Update($_POST['Id'], $_POST['Name'], $_POST['Description']);
            return new Redirect(URI_ROOT.'Subjects/');
        }
        $subject = SubjectService::ItemById($id);
        return new View($subject);
    }
    
    public function Delete($id){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            SubjectService::Delete($_POST['Id']);
            return new Redirect(URI_ROOT.'Subjects/');
        }
        $subject = SubjectService::ItemById($id);
        return new View($subject);
    }
}
