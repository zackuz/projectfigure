<?php
require_once ROOT.'/Components/Authenticate/RegisterModel.php';
require_once ROOT.'/Components/Data/DBService.php';


class AccountController {
    function Login(){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            if (Authorization::Login($_POST['Login'], $_POST['Password'], false)){
                $backUri = empty($_POST['BackUri'])?URI_ROOT:$_POST['BackUri'];
                return new Redirect($backUri);
            }
            else {
                $model = array(
                    'backUri' => $_POST['BackUri'],
                    'errors' => array('ALL' => array('Wrong login or password'))
                );
                return new View($model, null, 'Static');
            }
        }
        $backUri = (isset($_GET['back']) || !empty($_GET['back']))?$_GET['back']:URI_ROOT;
        $model = array(
            'backUri' => $backUri
        );
        
        return new View($model, null, 'Static');
    }
    
    function Register(){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            $backUri = empty($_POST['BackUri'])?URI_ROOT:$_POST['BackUri'];
            $errors = null;
            $validResult = RegisterModel::Validate($_POST['RegLogin'],
                    $_POST['RegName'], $_POST['RegPassword'], $_POST['PasswordConfirm']);
            if($validResult){//Model validation
                Authorization::RegisterAndLogin($validResult);
                return new View($validResult, ROOT."/Views/Home/Information.php", 'Hello');
            } 
            else {
                return new View(array(
                    'backUri' => $$backUri,
                    'errors' => array('ALL' => $errors)
                ), null,'Static');
            }
            
        }
        return new View(null, null,'Static');
    }
    
    function Logout(){
        Authorization::Logout();
        return new Redirect(URI_ROOT);
    }
    
    function Profile(){
        Authorization::DennyAnnonim();
        return new View();
    }
    
    public function Export()
    {
        Authorization::DennyAnnonim();
        $data = DBService::Export();
        return new JsonFile($data, 'EntyBase.json');
    }
    
    public function Import()
    {
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            if (!empty($_FILES['Base']['tmp_name'])){
                $result = DBService::ImportJson(json_decode(file_get_contents($_FILES['Base']['tmp_name'])));
                if (!Error::Is($result)) {
                    return new DataInfo($result);
                }
                else{
                    return $result;
                }
            }
            //if ($)
            return new Error('Bad file');
        }
    }
}

//class LoginModel
//{
//    $login;
//    $pass;
//}
