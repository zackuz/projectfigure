<?php
include_once ROOT.'/Components/Data/EntityService.php';

Authorization::DennyAnnonim();

class TestController{
    
    public function Index(){
        if ($_POST['Action'] == 'Delete') return new Text('It must be deleted!!!');
        if ($_POST['Action'] == 'Test') $model['REVERS']=false;
        if ($_POST['Action'] == 'Revers') $model['REVERS']=true;
        $model['DATA'] = EntityService::AllForUserBySets($_POST['Id']);
        
        return new View($model);
    }
    
    public function Revers($id=0){
        if ($id==0){
            return new DataInfo($_POST['Id']);
        }
        $model['REVERS']=true;
        $model['DATA'] = EntityService::AllForUserBySetId($id);
        return new View($model, ROOT.'/Views/Test/Index.php');
    }
    
    public function Avers($id=0){
                if ($id==0){
            return new DataInfo($_POST['Id']);
        }
        $model['REVERS']=false;
        $model['DATA'] = EntityService::AllForUserBySetId($id);
        return new View($model, ROOT.'/Views/Test/Index.php');
    }
    
}



