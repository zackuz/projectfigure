<?php
Authorization::DennyAnnonim();

include_once ROOT.'/Components/Data/EntityService.php';
include_once ROOT.'/Models/EntityModel.php';

class EntityController{
    
    public function Edit ($entityId = 0, $setId = 0){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            EntityService::Update($_POST['Id'], $_POST['Value'], $_POST['Link'], $_POST['SetId']);
            return new Redirect(URI_ROOT.$_POST['Back']);
        }
        $model['INFO'] = EntityService::ItemById($entityId);
        $model['BACK'] = $setId == 0?'Sets/':"Sets/View/$setId";
        $model['SET'] = $setId;
        return new View($model);
    }
    
    public function Delete ($entityId = 0, $setId = 0){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            EntityService::Delete($_POST['Id'], $setId);
            return new Redirect(URI_ROOT.$_POST['Back']);
        }
        $model['BACK'] = $setId == 0?'Sets/':"Sets/View/$setId";
        $model['ITEM'] = EntityService::ItemById($entityId);
        return new View($model);
    }
}

