<?php
    include_once ROOT.'/Models/SetModel.php';
    include_once ROOT.'/Components/Data/SetService.php';
    include_once ROOT.'/Components/Data/SubjectService.php';
    include_once ROOT.'/Components/Data/EntityService.php';
    
    Authorization::DennyAnnonim();

class SetsController {
    public function Index()
    {
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            return new DataInfo($_POST['Id']);
        }
        $data = SetService::AllGropeForUser();
        return new View($data);
    }
    
    public function Create(){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            $set = new SetModel(
                null,
                $_POST['Name'],
                $_POST['Description'],
                $_POST['SubjectId']);
        SetService::Create($set);
        return new Redirect(URI_ROOT.'Sets/');
        }
        $data['SUBS'] = SubjectService::AllForUser();
        return new View($data);
    }
    
    public function Edit($id){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            $set = new SetModel(
                $id,
                $_POST['Name'],
                $_POST['Description'],
                $_POST['SubjectId']);
            SetService::Update($set);
            return new Redirect(URI_ROOT.'Sets/');
        }
        $data['SUBS'] = SubjectService::AllForUser();
        $data['SET'] = SetService::ItemById($id);
        return new View($data);
    }
    
    public function View($setId) {
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            EntityService::InsertValues($_POST['Value'], $_POST['Link'], $setId);
        }
        $model['INFO'] = SetService::ItemById($setId);
        $model['LIST'] = EntityService::AllForUserBySetId($setId);
        return new View($model);
    }

    public function Delete($id){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            SetService::Delete($_POST['Id']);
            return new Redirect(URI_ROOT.'Sets/');
        }
        $set = SetService::ItemById($id);
        return new View($set);
    }
    
    public function Entity ($entityId = 0, $setId = 0){
        if ($_SERVER['REQUEST_METHOD']=='POST'){
            //Save
            if ($setId == 0) return new Redirect(URI_ROOT.'Sets/');
               else return new Redirect(URI_ROOT."Sets/View/$setId");
        }
        $model = EntityService::ItemById($entityId);
        return new DataInfo($model);
    }
    
    public function Join ($sourceId = 0, $rootId = 0){
        if ($rootId == 0) {
            $model['LIST'] = SetService::AllGropeForUserButId($sourceId);
            $model['ID'] = $sourceId;
            return new View($model);
        }
        SetService::Gather($rootId, $sourceId);
        return new Redirect(URI_ROOT.'Sets/');
    }
}
